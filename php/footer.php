<link rel="stylesheet" type="text/css" href="css/footer.css">
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12">
				<ul class="nav">
					<li><a class="nav-link"  href="index.php">INICIO</a></li>
					<li><a class="nav-link" href="about.php">NOSOTROS</a></li>
					<li><a class="nav-link" href="gallery.php">FOTOS</a></li>
					<li><a class="nav-link" href="event.php">EVENTO</a></li>
					<li><a class="nav-link" href="gift.php">REGALOS</a></li>
				</ul>
			</div>

			<div class="logos col-md-4 col-sm-12">
				<div class="row pull-right ">
					<div class="power">Diseño por:</div>
					<div><a href="https://www.behance.net/matiasfoscf067" target="_blank"><img id="mati" src="img/Logo_02.png" title="Matias Fosco"  alt="Matias Fosco"/></a></div>
					<div><a href="https://www.behance.net/adrianvill31a7" target="_blank"><img id="adri" src="img/Logo_01.png" title="Adrian Villamayor" alt="Adrian Villamayor"></a></div>
				</div>
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript" src="script/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="script/plugin.js"></script>
<script type="text/javascript" src="script/popper.js"></script>
<script type="text/javascript" src="script/bootstrap.js"></script>
<script type="text/javascript" src="script/contador.js"></script>
<script type="text/javascript" src="script/script.js"></script>
<script type="text/javascript" src="script/jquery.validate.min.js"></script>
<script type="text/javascript" src="script/validator.js"></script>