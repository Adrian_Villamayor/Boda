<link rel="stylesheet" type="text/css" href="css/banner.css">

<section id="toConf">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-lg-9 col-md-8 col-sm-12" id="conf">Confirmanos tu asistencia, queremos que compartas este gran día con nosotros</div>
		
			<div class="col-xl-4 col-lg-3 col-md-4 col-sm-12 " id="confButt"><button class="aOscuro" data-toggle="modal" data-target="#exampleModal"><strong>Confirmar asistencia</strong></button></div>
		</div>
	</div>
</section> 
