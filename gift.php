<html lang="es-ES">
<head>
	<title>Evento</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="https://fonts.googleapis.com/css?family=Courgette|Open+Sans|Pacifico|Playfair+Display" rel="stylesheet">	
	<link rel="icon" href="img/log.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/gift.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
	
	<!-- ...................................................................................... -->
	<?php include("php/nav.php") ?>
	<!-- ...................................................................................... -->
	<header>
		<div class="container">
			<div class="row content">
				<div class="col-md-6">
					<p class="sub rosa">Regalos</p>
					<p class="tit blanco ">Un viaje de novios inolvidable</p> 
					<div class="parr blanco">
						<p>Queremos que lo importante sea el que nos acompañéis a celebrar con ilusión, alegría y muchas ganas de disfrutar el día de nuestra boda. Agradecemos de corazón si nos ayudáis en la contribución para el viaje de novios de nuestros sueños.</p>
					</div>
					<a id="toGift" class="aOscuro1"><b>Haz tu regalo</b></a>
				</div>
			</div>
		</div>
		<?php include("php/banner.php"); ?>
		
	</header>
	<!-- ...................................................................................... -->


	<!-- ...................................................................................... -->
	<section id="giftInf">
		<div class="container">
			<div class="row"> 
				<div class="col-md-5"> 
					<p class="sub aling">El Convite </p>
					<p class="tit aling">Castell de Sant Marçal</p> 
					<div class="parr aling">
						<p>A las 19h empezará el aperitivo en este majestuoso castillo en los jardines inferiores. Para aquellos que decidáis venir en coche, el castillo dispone de aparcamiento gratuito dentro del propio establecimiento.
							Terminado el aperitivo accederemos al Invernadero del Castillo para disfrutar de una suculenta cena y de varias sorpresas que hará que cada uno de vosotros se sienta único y especial.
							Y eso no es todo, porque al final contaremos con un magnífico DJ.
							Esperamos que vengáis con mucha energía y estómago para disfrutar de este día tan especial para nosotros. Así que ya lo sabéis, ¡os esperamos!
						</p>
					</div>
				</div>

				<div class="col-md-2"> 
					
				</div>

				<div class="col-md-5"> 
					<img src="img/arms.jpg">
				</div>
			</div>
			<div class="row"> 
				
				<div class="col-md-5" style="left: -200px;"> 
					<img src="img/arms.jpg">
				</div>
				<div class="col-md-2"> 
				</div>

				<div class="col-md-5"> 
					<p class="sub ">El Convite </p>
					<p class="tit ">Castell de Sant Marçal</p> 
					<div class="parr ">
						<p>A las 19h empezará el aperitivo en este majestuoso castillo en los jardines inferiores. Para aquellos que decidáis venir en coche, el castillo dispone de aparcamiento gratuito dentro del propio establecimiento.
							Terminado el aperitivo accederemos al Invernadero del Castillo para disfrutar de una suculenta cena y de varias sorpresas que hará que cada uno de vosotros se sienta único y especial.
							Y eso no es todo, porque al final contaremos con un magnífico DJ.
							Esperamos que vengáis con mucha energía y estómago para disfrutar de este día tan especial para nosotros. Así que ya lo sabéis, ¡os esperamos!
						</p>

					</div>
				</div>
			</div>
			<div class="row"> 
				
				<div class="col-md-5"> 
					<p class="sub aling">El Convite </p>
					<p class="tit aling">Castell de Sant Marçal</p> 
					<div class="parr aling">
						<p>A las 19h empezará el aperitivo en este majestuoso castillo en los jardines inferiores. Para aquellos que decidáis venir en coche, el castillo dispone de aparcamiento gratuito dentro del propio establecimiento.
							Terminado el aperitivo accederemos al Invernadero del Castillo para disfrutar de una suculenta cena y de varias sorpresas que hará que cada uno de vosotros se sienta único y especial.
							Y eso no es todo, porque al final contaremos con un magnífico DJ.
							Esperamos que vengáis con mucha energía y estómago para disfrutar de este día tan especial para nosotros. Así que ya lo sabéis, ¡os esperamos!
						</p>
					</div>
				</div>
				<div class="col-md-2"> 
				</div>


				<div class="col-md-5"> 
					<img src="img/arms.jpg">
				</div>
			</div>
		</div>
	</section>
	<!-- ...................................................................................... -->
	<link rel="stylesheet" type="text/css" href="css/confirmar.css">

	<section id="formul">
		<div class="container">
			<p class="sub">Confirmar asistencia</p>
			<p class="tit">COMPARTE ESTE DIA CON NOSOTROS</p>	 

			<div class="row">
				<div class="col-md-3 form-group">
					<label for="name">Nombre</label>
					<input class="form-control" type="text" name="name" id="name">
				</div>
				<div class="col-md-3 form-group">
					<label for="surn">Apellido</label>
					<input class="form-control" type="text" name="surn" id="surn">
				</div>
				<div class="col-md-3 form-group">
					<label for="email">Correo electronico</label>
					<input class="form-control" type="email" name="email" id="email">
				</div>
				<div class="col-md-3 form-group">
					<label for="pago">Importe</label>
					<input class="form-control" type="number" name="pago" id="pago">
				</div>
			</div>
			<div class="row" id="regaloBox">
				<a id="regalos" class="aOscuro1"><b>Regalar</b></a>
			</div>

		</div>

	</section>

	<!-- ...................................................................................... -->

	<?php include("php/footer.php"); ?>

</body>
</html>


<script type="text/javascript">

	$("#regalos").click(function(){

		if($("#pago").val()!=""){
			var a = $("#pago").val();
			$.ajax({
				url: "./php/post.php", 
				data: {a:a},
				type: 'post',
				success: function(result){
					window.open(result, '_blank');
				}
			});
		}
	});

	var value = Math.round((130*100)/365);
	$(".my-progress-bar").circularProgress({
		line_width: 3,
		color: "#ff6347",
		starting_position: 0, 
		percent: 0, 
		percentage: true
	}).circularProgress('animate', value, 1000);

</script>


