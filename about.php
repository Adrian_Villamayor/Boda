<html lang="es-ES">
<head>
	<title>Nosotros</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="https://fonts.googleapis.com/css?family=Courgette|Open+Sans|Pacifico|Playfair+Display:400,700&amp;subset=latin-ext" rel="stylesheet">		
	<link rel="icon" href="img/log.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/about.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
	
	<!-- ...................................................................................... -->
	<?php include("php/nav.php") ?>
	<!-- ...................................................................................... -->
	<header>
		<div class="container">
			<div class="col-xl-6 col-lg-7 col-md-12 col-sm-12 cont">
				<p class="sub rosa">Nosotros</p>
				<p class="tit blanco">Amor a primera vista</p>
				<div class="parr blanco">
					<p>Nos conocimos en el 2013 y desde entonces no nos hemos separado. 
						Uno no sabe nunca dónde puede encontrar a su media naranja, pero a veces son los lugares más inhóspitos los que nos sorprenden. En nuestro caso, fue la empresa en la que trabajábamos, las largas charlas afterwork y coincidir en un proyecto lo que hizo saltar la chispa.
						Fueron nuestras ganas de viajar, de conocer mundo, nuestro amor por el arte culinario y musical, así como los valores que compartíamos, lo que nos enamoró.
					</p>
				</div>
			</div>
		</div>
		<?php include("php/banner.php"); ?>
		
	</header>
	<!-- ...................................................................................... -->

	
	<!-- ...................................................................................... -->
	<section id="aboutInf">
		<div class="container">

			<div class="row"> 
				<div id="out" class="col-xl-7 col-lg-6 col-md-12 col-sm-12">
					<img src="img/huge.jpg">
				</div>
				<div class="col-xl-5 col-lg-6 col-md-12 col-sm-12">
					<p class="sub">Nuestra fuerza en común </p>
					<p class="tit">El principio de todo</p> 
					<div class="parr">
						<p>Para aquellos que nos conocéis de primera mano ya sabéis lo diferentes que somos. Precisamente por eso, hemos apostado tan fuerte por nuestra relación. Porque nos complementamos en nuestras debilidades y flaquezas, porque aprendemos el uno del otro, porque asimilamos situaciones desde puntos de vista muy diversos. Pero, sobre todo, porque nos queremos igual que el primer día. </p>
					</div>
				</div>

			</div>

			<div class="row vino"> 
				<div class="col-xl-5 col-lg-7 col-md-12 col-sm-12" id="paddi">
					<p class="sub">Pasito a pasito </p>
					<p class="tit blanco">Forjando nuestra relación</p> 
					<div class="parr blanco">
						<p>Es muy complicado resumir los cinco años que hemos pasado juntos, pero si tenemos que quedarnos con algo, es con todos los lugares tan maravillosos que hemos descubierto juntos. La lista es extensa: Bélgica (nuestro primer viaje en pareja), Japón (el destino que más nos enamoró), China (en familia), Islandia (naturaleza pura), costa este de Estados Unidos (vuelta a “casa” de nuevo) y Sudáfrica (safaris y animales increíbles) entre otros  </p>
					</div>
				</div>

				<div class="col-xl-7 col-lg-5 col-md-12 col-sm-12" id="upTo">
					<img src="img/arms.jpg">
				</div>


			</div>

			<div class="row">
				<div class="col-xl-7 col-lg-6 col-md-12 col-sm-12"></div>
				<div class="col-xl-5 col-lg-6 col-md-12 col-sm-12">
					<p class="sub">Nuestro SÍ QUIERO </p>
					<p class="tit">Avanzando hacia el futuro</p> 
					<div class="parr">
						<p>Tras más de cinco años juntos, tres viviendo en pareja y un montón de experiencias a nuestras espaldas, hemos decidido dar el gran paso y darnos el Sí Quiero el próximo sábado 12 de Mayo de 2018 en Barcelona. 
						Juntos formamos un gran equipo, pero sin vosotros estaríamos incompletos. Por eso, nada nos hace más ilusión que compartir este día tan especial con vosotros. ¡Os esperamos!</p>
					</div>
				</div>

			</div>

		</div>

	</section>
	<!-- ...................................................................................... -->
	<?php include("php/form.php"); ?>

	<!-- ...................................................................................... -->

	<?php include("php/footer.php"); ?>

</body>
</html>