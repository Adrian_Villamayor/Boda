<html lang="es-ES">
<head>
	<title>Galeria</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="https://fonts.googleapis.com/css?family=Courgette|Open+Sans|Pacifico|Playfair+Display" rel="stylesheet">	
	<link rel="icon" href="img/log.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/gallery.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	
</head>
<body>
	

	<!-- ...................................................................................... -->
	<?php include("php/nav.php") ?>
	<!-- ...................................................................................... -->
	<header>
		<div class="container">
			<div class="col-md-6"></div>
			<div class="col-md-6 content">
				<p class="sub rosa">Fotos</p>
				<p class="tit blanco" id="shrink">Una vida en imagenes</p>
				<div class="parr blanco">
					<p>Conoced cómo éramos de pequeños, nuestra primera andadura escolar, el complicado momento de la pubertad y nuestra evolución como adultos y pareja. Algunas imágenes han permanecidas ocultas durante mucho tiempo… ¡hasta ahora! </p>
				</div>
			</div>
		</div>
		<?php include("php/banner.php"); ?>
		
	</header>
	<!-- ...................................................................................... -->



	<!-- ...................................................................................... -->
	<section id="extImg">
		<div class="container">

			<div class="grid">
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
				<div class="grid-cell">a</div>
			</div>

		</div>

	</section>
	<!-- ...................................................................................... -->
	<?php include("php/form.php"); ?>

	<!-- ...................................................................................... -->
	<?php include("php/footer.php"); ?>
</body>
</html>