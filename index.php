<html lang="es-ES">
<head>
	<title>Inicio</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="https://fonts.googleapis.com/css?family=Courgette|Open+Sans|Pacifico|Playfair+Display:400,700&amp;subset=latin-ext" rel="stylesheet">	
	<link href="https://fonts.googleapis.com/css?family=Arapey|Prata" rel="stylesheet">
	<link rel="icon" href="img/log.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">


</head>
<body onload="calcula('contador')">

	<!-- ...................................................................................... -->
	
	<?php include("php/nav.php") ?>

	<!-- ...................................................................................... -->

	<header>
		<div class="container">
			<div class="content">
				<div id='contador'></div>
				<div class="row">
					<div class="col-md-12 fecha">12 de Mayo de 2018</div>

					<div class="shrink col-xl-12">¡Bienvenidos a nuestra página Web!<br> Aquí encontraréis toda la información que debéis conocer para el día de nuestra boda.</div>
				</div>
			</div>
		</div>
		<!-- ...................................................................................... -->
		<?php include("php/banner.php"); ?>
		<!-- ...................................................................................... -->
	</header>


	<section id="about">
		<div class="container">
			<div class="row">
				<div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 nos">
					<p class="sub">Nosotros</p>
					<p class="tit blanco">Amor a primera vista</p>
					<div class="parr blanco">
						<p>Muchos de vosotros nos conocéis de primera mano; Otros, quizás un poquito menos. Os invitamos a descubrir cómo nos conocimos, lo que nos une y lo que nos motiva a seguir avanzando juntos en este viaje que nos regalará grandes momentos de felicidad.</p>
					</div>
					<a class="aRosa" href="./about.php"><strong>más información</strong></a>
				</div>
				<div class="col-xl-5 col-lg-5 nos-img">
					<img src="img/about.png">
				</div>
				
			</div>
		</div>
	</section>

	<!-- ...................................................................................... -->
	<section id="extInf">
		<div class="container">
			<section id="galery">
				<div class="row">
					<div class="col-xl-5 col-lg-5">
						<img src="img/fotos.png">
					</div>
					<div class="fotos col-lg-7 col-md-12 col-sm-12">
						<p class="sub">Fotos</p>
						<p class="tit">UNA VIDA EN IMAGENES</p> 
						<div class="parr">
							<p>Si queréis saber cómo éramos de pequeños, nuestro paso por la adolescencia, nuestros momentos más divertidos de juventud y nuestras primeras imágenes juntos, no dudéis en hacer click en este enlace. ¡Estamos seguros de que a más de uno os sacará una sonrisa!</p>
						</div>
						<a class="aOscuro" href="gallery.php"><strong>Más Información</strong></a>
					</div>
				</div>

			</section>

			<section id="event">
				<div class="row">
					<div class="evento col-xl-5 col-lg-6 col-md-12 col-sm-12">
						<p class="sub">Evento</p>
						<p class="tit">EL LUGAR SOÑADO</p> 
						<div class="parr">
							<p>Para toda la información referente al evento, lugar de la ceremonia y posterior banquete, entrad en el siguiente enlace. Allí obtendréis todo lo que necesitáis saber en relación al horario establecido, la localización de la iglesia y del convite, así como de la organización de este día tan especial.</p>
						</div>
						<a class="aOscuro" href="event.php"><strong>Más Información</strong></a>
					</div>
				</div>
			</section>
		</div>
	</section>

	<!-- ...................................................................................... -->
	<?php include("php/form.php"); ?>

	<!-- ...................................................................................... -->

	<?php include("php/footer.php"); ?>
	
</body>
</html>


<script type="text/javascript">


	
	var value = Math.round((130*100)/365);
	$("#dia").circularProgress({
		line_width: 3,
		color: "#ff6347",
		starting_position: 0, 
		percent: 0, 
		percentage: true
	}).circularProgress('animate', value, 1000);
</script>