<html lang="es-ES">
<head>
	<title>Evento</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="https://fonts.googleapis.com/css?family=Courgette|Open+Sans|Pacifico|Playfair+Display" rel="stylesheet">	
	<link rel="icon" href="img/log.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/event.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	

</head>
<body>
	
	<!-- ...................................................................................... -->
	<?php include("php/nav.php") ?>
	<!-- ...................................................................................... -->
	<header>
		<div class="container">
			<div class="col-xl-6 col-lg-7 col-md-12 col-sm-12 cont">
				<p class="sub rosa">Nosotros</p>
				<p class="tit blanco">Amor a primera vista</p>
				<div class="parr blanco">
					<p>La boda se celebrará mediante un acto religioso en la ciudad de Barcelona y el banquete se llevará a cabo en un castillo precioso en Cerdanyola del Vallés. Se trata de una boda de tarde, por lo que recomendamos que os traigáis ropa de abrigo por si en el aperitivo refrescara. Aquellas chicas que lo deseen pueden traer un segundo par de zapatos para que la hora del baile no sea un impedimento para nadie. Aunque ya os adelantamos que habrá más de una sorpresa…
					</p>
				</div>
			</div>
		</div>
	<?php include("php/banner.php"); ?>
		
	</header>
	<!-- ...................................................................................... -->


	<!-- ...................................................................................... -->
	<section id="eventInf">
		<div class="container">

			<div class="row"> 
				<div id="out" class="col-xl-7 col-lg-6 col-md-12 col-sm-12">
					<img src="img/huge.jpg">
				</div>
				<div class="col-xl-5 col-lg-6 col-md-12 col-sm-12">
					<p class="sub">La Ceremonia </p>
					<p class="tit">Santuari de Santa María de Montserrat de Pedralbes</p> 
					<div class="parr">
						<p>La boda religiosa tendrá lugar a las 17h del 12 de Mayo del 2018 en el Santuari de Santa Maria de Montserrat de Pedralbes. Recomendamos que estéis allí por lo menos media hora antes de la celebración del enlace para que os situéis, toméis asiento y empecemos puntuales la celebración.
							Hay estacionamiento gratuito en las inmediaciones de la Iglesia por lo que no os resultará complicado aparcar. 
						</p>
					</div>
				</div>

			</div>

			<div class="row vino"> 
				<div class="col-xl-6 col-lg-7 col-md-12 col-sm-12" id="paddi">
					<p class="sub">El Convite </p>
					<p class="tit blanco">Castell de Sant Marçal</p> 
					<div class="parr blanco">
						<p>A las 19h empezará el aperitivo en este majestuoso castillo en los jardines inferiores. Para aquellos que decidáis venir en coche, el castillo dispone de aparcamiento gratuito dentro del propio establecimiento.
							Terminado el aperitivo accederemos al Invernadero del Castillo para disfrutar de una suculenta cena y de varias sorpresas que hará que cada uno de vosotros se sienta único y especial.
							Y eso no es todo, porque al final contaremos con un magnífico DJ.
							Esperamos que vengáis con mucha energía y estómago para disfrutar de este día tan especial para nosotros. Así que ya lo sabéis, ¡os esperamos!
						</p>
					</div>
				</div>
				<div class="col-xl-6 col-lg-5 col-md-12 col-sm-12" id="upTo">
					<img src="img/arms.jpg">
				</div>
			</div>

			<div class="row"> 
				<div class="col-xl-7 col-lg-6 col-md-12 col-sm-12"></div>
				<div class="col-xl-5 col-lg-6 col-md-12 col-sm-12">
					<p class="sub">La Ceremonia </p>
					<p class="tit">Transporte</p> 
					<div class="parr">
						<p>Para facilitar la conexión entre ambos puntos y permitir que todos los invitados puedan disfrutar de la boda sin restricciones (cocktails, vino, cava y algunas copas de más) pondremos a vuestra disposición un autocar que partirá de la Iglesia una vez terminada la ceremonia y llevará a aquellas personas que así lo indiquen en el email de confirmación directamente a Cerdanyola.
							La vuelta de Cerdanyola a Barcelona se realizará en distintos turnos para amoldarse mejor a las necesidades de cada uno de vosotros y os dejará en XX. Todo lo referente a horarios se comunicará directamente el día de la boda a través del conductor.
						</p>
					</div>
				</div>

			</div>
		</div>

	</section>
	<section id="maps">
		<div class="row">
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.158728181054!2d2.1115556559854176!3d41.39235915128716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4984e17ba99bb%3A0xfbd9adc8bb53a4!2sSantuari+de+Santa+Maria+de+Montserrat+de+Pedralbes!5e0!3m2!1ses!2ses!4v1507904898857" width="100%" height="600px" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2988.8398730200097!2d2.115921615759114!3d41.48607109783392!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a29cc7b19113%3A0x89be5f39ac9a67de!2sCastillo+de+San+Marcial!5e0!3m2!1ses!2ses!4v1507904962816" width="100%" height="600px" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</section>
	<!-- ...................................................................................... -->
	<?php include("php/form.php"); ?>


	<!-- ...................................................................................... -->

	<?php include("php/footer.php"); ?>

</body>
</html>