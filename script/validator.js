$(function () {
    //Relación con id validation2
    $('#validation').validate({
        debug: false,
        //Reglas que deben respetar los inputs
        rules: {
            "nom": {
                required: true
            },
            "mail": {
                required: true,
                email: true
            },
            "cog": {
                required: true,
            },
            "mensaje": {
                required: true
            }

        },
        //Mensajes de alerta al introducir mal el valor de un input
        messages: {
            "nom": {
                required: "Introdueix un nom"
            },
            "mail": {
                required: "Introdueix un correu vàlid",
                email: "Siusplau, introdueixi una adreça vàlida"
            },
            "cog": {
                required: "Introdueix un cognom"
            },
            "mensaje": {
                required: "Fes la teva consulta"
            } 
        },
        submitHandler: function(form) {
            $.ajax({
                type: "POST",
                url: "php/form.php",
           data: $("#validation").serialize(), // Adjuntar los campos del formulario enviado.
           success: function(data){
               $("#result").html(data); // Mostrar la respuestas del script PHP.
           }
       });
        }
    });
});