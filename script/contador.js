function calcula(id){
	var fecha=new Date('2018','4','12','12','00','00')
	var hoy=new Date()
	var dias=0
	var horas=0
	var minutos=0
	var segundos=0
	
	if (fecha>hoy){
		var diferencia=(fecha.getTime()-hoy.getTime())/1000
		dias=Math.floor(diferencia/86400)
		diferencia=diferencia-(86400*dias)
		horas=Math.floor(diferencia/3600)
		diferencia=diferencia-(3600*horas)
		minutos=Math.floor(diferencia/60)
		diferencia=diferencia-(60*minutos)
		segundos=Math.floor(diferencia)
		
		document.getElementById(id).innerHTML='<div class="circulo" id="dia"> <span class="big">'
		+dias + ' </span> </br> <span> D&iacute;as </span></div> <div class="circulo" id="hora"><span class="big">'
		+ horas + ' </span> </br> <span> Horas </span></div> <div class="circulo" id="minutos"><span class="big">'
		+ minutos + '</span> </br> <span> Minutos </span> </div> <div class="circulo"> <span class="big">'
		+ segundos + '</span> </br>  <span>Segundos  </span></div>'
		
		if (dias>0 || horas>0 || minutos>0 || segundos>0){
			setTimeout("calcula(\"" + id + "\")",1000)
		}
	}
	else{
		document.getElementById('restante').innerHTML='<div class="circulo" id="dia"> <span class="big">'
		+dias + ' </span> </br> <span> D&iacute;as </span></div> <div class="circulo"><span class="big">'
		+ horas + ' </span> </br> <span> Horas </span></div> <div class="circulo"><span class="big">'
		+ minutos + '</span> </br> <span> Minutos </span> </div> <div class="circulo"> <span class="big">'
		+ segundos + '</span> </br>  <span>Segundos  </span></div>'
	}
}